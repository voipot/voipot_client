package voipot.jnu.conoz.com.voipotclient.util

import android.annotation.SuppressLint
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.DateUtil
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import voipot.jnu.conoz.com.voipotclient.vo.ScheduleVO
import java.io.FileInputStream
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

class ExcelUtil {
    val resultVOList: LinkedList<ScheduleVO> = LinkedList()
        get() {
            if (field.size == 0) {
                RuntimeException("fileOpen()을 이용해서 파일을 읽어주세요")
            }
            return field
        }

    @SuppressLint("SimpleDateFormat")
    fun fileOpen(filePath: String) {
        val fileExtension = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length)
        val scheduleFile = FileInputStream(filePath)

        if (fileExtension == "xls") {
            val workBook = HSSFWorkbook(scheduleFile)
            val sheet = workBook.getSheetAt(0)
            val row = sheet.physicalNumberOfRows

            for (i in row downTo 0) {
                val scheduleVO = ScheduleVO()
                scheduleVO.time = SimpleDateFormat("HH:mm").format(DateUtil.getJavaDate(sheet.getRow(i).getCell(0).numericCellValue))
                scheduleVO.title = sheet.getRow(i).getCell(1).stringCellValue
                scheduleVO.memo = sheet.getRow(i).getCell(2).stringCellValue

                resultVOList.add(scheduleVO)
            }
        } else if (fileExtension == "xlsx") {
            val workBook = XSSFWorkbook(scheduleFile)
            val sheet = workBook.getSheetAt(0)
            val row = sheet.physicalNumberOfRows

            println("sheet : $sheet")

            for (i in 0 until row) {
                val scheduleVO = ScheduleVO()
//                println("i : $i , cell count : " +sheet.getRow(i).physicalNumberOfCells)

                try {
                    scheduleVO.time = SimpleDateFormat("HH:mm").format(DateUtil.getJavaDate(sheet.getRow(i).getCell(0).numericCellValue))
                    scheduleVO.title = sheet.getRow(i).getCell(1).stringCellValue
                    scheduleVO.memo = sheet.getRow(i).getCell(2).stringCellValue
                    println("time : ${scheduleVO.time}")
                    println("title : ${scheduleVO.title}")
                    println("memo : ${scheduleVO.memo}")
                } catch (e: NullPointerException) {
                    System.err.println(e.toString())
                    e.printStackTrace()
                }

                resultVOList.add(scheduleVO)
            }
        }
    }
}
