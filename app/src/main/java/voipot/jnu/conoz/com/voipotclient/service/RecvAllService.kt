package voipot.jnu.conoz.com.voipotclient.service

import android.app.IntentService
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import voipot.jnu.conoz.com.voipotclient.CommonApp
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket

//const val ACTION_RECVMESSAGE_RESULT = "ACTION_RECVMESSAGE_RESULT"
//const val KEY_NAME_RESULT = "KEY_NAME_RESULT"
//const val KEY_TIME_RESULT = "KEY_TIME_RESULT"
//const val KEY_MESSAGE_RESULT = "KEY_MESSAGE_RESULT"

class RecvAllService : IntentService("RecvAllService") {
    private var isOn = true
    override fun onHandleIntent(intent: Intent?) {
        var socket: DatagramSocket? = null

        try {
            socket = DatagramSocket(CommonApp.PORT_ALL_RECV)
            var datagramPacket: DatagramPacket
            while (isOn) {
                val packetBuffer = ByteArray(128)
                datagramPacket = DatagramPacket(packetBuffer, 128)
                socket.receive(datagramPacket)

                if (datagramPacket.data.isNotEmpty()) {
                    val address = datagramPacket.address.hostAddress.toString()

                    //가이드로부터만 받을 수 있게.
                    if (address == CommonApp.guideIP) {
                        for ((i, bb) in datagramPacket.data.withIndex()) {
                            if (bb == 0.toByte()) {
                                ByteArray(i).apply {
                                    for (k in 0..(i - 1)) {
                                        set(k, datagramPacket.data[k])
                                    }
                                }.let {
                                    datagramPacket.data = it
                                }
                                break
                            }
                        }

                        String(datagramPacket.data, Charsets.UTF_8).let { s ->
                            val recvData = s.split("|,")
                            val recvMessage = recvData.asSequence()
                                    .joinToString()

                            //1 보다 작은것은 관광객이 보낸것
//                            if (recvData.size > 1) {
//                                LocalBroadcastManager.getInstance(applicationContext)
//                                        .sendBroadcast(Intent(ACTION_RECVMESSAGE_RESULT).apply {
//                                            putExtra(KEY_MESSAGE_RESULT, recvMessage)
//                                            putExtra(KEY_NAME_RESULT, recvData[0])
//                                            putExtra(KEY_TIME_RESULT, recvData[1])
//                                        })
//                            }

                        }
                    }
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
            Log.e(this.toString(), "Exception: " + e.message)
        } catch (e: InterruptedException) {
            e.printStackTrace()
            Log.e(this.toString(), "Exception: " + e.message)
        } finally {
            socket?.close()
        }

//        //핫스팟에 연결된 클라이언트들 정보 얻어옴.
//        println("start redvMessage")
//        while (isOn) {
//            val socket = DatagramSocket(CommonApp.PORT_MESSAGE)
//            try {
//                while (true) {
//                    val packetBuffer = ByteArray(512)
//                    val datagramPacket = DatagramPacket(packetBuffer, 512)
//                    socket.receive(datagramPacket)
//
//                    if (datagramPacket.data.isNotEmpty()) {
//                        val address = datagramPacket.address.hostAddress.toString()
//
//                        //채워지지 않은 배열 지우기
//                        for ((i, bb) in datagramPacket.data.withIndex()) {
//                            if (bb == 0.toByte()) {
//                                ByteArray(i).apply {
//                                    for (k in 0..(i - 1)) {
//                                        set(k, datagramPacket.data[k])
//                                    }
//                                }.let {
//                                    datagramPacket.data = it
//                                }
//                                break
//                            }
//                        }
//
//                        val recvData = String(datagramPacket.data, Charsets.UTF_8)
//
//                        println("recvMessage : $recvData")
//                        listener!!.onReceiveMessage(recvData, address)
//                        LocalBroadcastManager.getInstance(applicationContext)
//                                .sendBroadcast(Intent(ACTION_RECVMESSAGE_RESULT).apply {
//                                    putExtra(KEY_MESSAGE_RESULT, recvData)
//                                    putExtra(KEY_NAME_RESULT, address)
//                                })
//                    }
//                }
//            } catch (e: IOException) {
//                e.printStackTrace()
//                Log.e(this.toString(), "Exception: " + e.message)
//            } catch (e: InterruptedException) {
//                e.printStackTrace()
//                Log.e(this.toString(), "Exception: " + e.message)
//            } finally {
//                socket.close()
//            }
//
//            Thread.sleep(5000)
//        }
    }

    override fun onDestroy() {
        isOn = false
        super.onDestroy()
    }
}
