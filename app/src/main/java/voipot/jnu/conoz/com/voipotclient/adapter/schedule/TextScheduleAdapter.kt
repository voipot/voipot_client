package voipot.jnu.conoz.com.voipotclient.adapter.schedule

import android.content.Context
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_schedule.view.*
import voipot.jnu.conoz.com.voipotclient.R
import voipot.jnu.conoz.com.voipotclient.adapter.ImgPagerAdapter
import voipot.jnu.conoz.com.voipotclient.vo.ScheduleVO

class TextScheduleAdapter(context: Context, scheduleData: List<ScheduleVO>) : ScheduleRecyclerAdapter<TextScheduleAdapter.ScheduleHolder>(context, scheduleData) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleHolder {
        return ScheduleHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_schedule, parent, false))
    }

    override fun onBindViewHolder(holder: ScheduleHolder, position: Int) {
        holder.time.text = scheduleData[position].time
        holder.title.text = scheduleData[position].title
        holder.memo.text = scheduleData[position].memo

        if (scheduleData[position].imgList == null) {
            holder.imgsLayout.visibility = ViewGroup.GONE
        } else {
            holder.imgMaxPage = (scheduleData[position].imgList!!.size - 1) / 5

            if (holder.imgMaxPage == 0) {
                holder.btnNextImg.visibility = ViewGroup.GONE
            }

            holder.imgsLayout.visibility = ViewGroup.VISIBLE
            holder.imgsPager.adapter = ImgPagerAdapter(scheduleData[position].imgList, scheduleData[position].id!!)
        }

        with(holder) {
            imgsPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    imgCurrentPage = position

                    if (imgCurrentPage >= imgMaxPage) {
                        btnNextImg.visibility = ViewGroup.GONE
                    }

                    if (position <= 0) {
                        btnPreImg.visibility = ViewGroup.GONE

                        if (btnNextImg.visibility == ViewGroup.GONE) {
                            btnNextImg.visibility = ViewGroup.VISIBLE
                        }
                    } else if (position >= imgMaxPage) {
                        btnNextImg.visibility = ViewGroup.GONE

                        if (btnPreImg.visibility == ViewGroup.GONE) {
                            btnPreImg.visibility = ViewGroup.VISIBLE
                        }
                    } else if (position == 1 || position == imgMaxPage - 1) {
                        btnNextImg.visibility = ViewGroup.VISIBLE
                        btnPreImg.visibility = ViewGroup.VISIBLE
                    }
                }
            })

            btnNextImg.setOnClickListener {
                imgsPager.currentItem = imgCurrentPage + 1
            }
            btnPreImg.setOnClickListener {
                imgsPager.currentItem = imgCurrentPage - 1
            }
        }
    }

    class ScheduleHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val time = itemView.txtTime!!
        val title = itemView.txtTitle!!
        val memo = itemView.txtMemo!!

        val imgsLayout = itemView.imgsLayout!!
        val imgsPager = itemView.imgsPager!!
        val btnPreImg = itemView.btnPreImg!!
        val btnNextImg = itemView.btnNextImg!!
        var imgCurrentPage = 0
        var imgMaxPage = 0
    }
}