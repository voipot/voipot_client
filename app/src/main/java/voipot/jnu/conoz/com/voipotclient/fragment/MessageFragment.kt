package voipot.jnu.conoz.com.voipotclient.fragment


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_message.*
import voipot.jnu.conoz.com.voipotclient.CommonApp
import voipot.jnu.conoz.com.voipotclient.R
import voipot.jnu.conoz.com.voipotclient.adapter.MessageRecyclerAdapter
import voipot.jnu.conoz.com.voipotclient.transport.MessageUDP
import voipot.jnu.conoz.com.voipotclient.util.MessageDbHelper
import voipot.jnu.conoz.com.voipotclient.vo.MessageVO
import java.util.*

class MessageFragment : Fragment() {
    private val TAG = "MessageFragment"
    private var isExpand = false
    private var messagePage = 1
    private lateinit var messageDbHelper: MessageDbHelper

    private val imm by lazy {
        activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private val messageRecyclerAdapter by lazy { MessageRecyclerAdapter(LinkedList()) }

    companion object {
        @JvmStatic
        fun newInstance() =
                MessageFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        messageDbHelper = MessageDbHelper(activity!!)
        arguments?.let {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_message, container, false)

        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        if (isVisibleToUser) {
            println("MessageFragment 보인다")
            messagePage = 1
            messageRecyclerAdapter.addAllProvider(messageDbHelper.getMessages(messagePage))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setSmallLayout()
        recyclerMessage.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, true)
        recyclerMessage.adapter = messageRecyclerAdapter

        recyclerMessage.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val lastVisiblePosition = (recyclerView!!.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                val count = recyclerView.adapter.itemCount

                if (lastVisiblePosition == count) {
                    messagePage++
                    (recyclerView.adapter as MessageRecyclerAdapter).addOldMessages(messageDbHelper.getMessages(messagePage))
                }
            }
        })

        if (isExpand) {
            smallLayout.visibility = ViewGroup.GONE
            recyclerMessage.visibility = ViewGroup.VISIBLE
        }

        btnSend.setOnClickListener { _ ->
            editMessage.text.toString().let { editMessage ->
                val messageUDP = MessageUDP()

                messageUDP.sendMessage(editMessage)

                MessageVO().apply {
                    name = "나"
                    message = editMessage
                    time = CommonApp.getCurrentTime()
                    isRead = true
                }.let {
                    messageDbHelper.add(it)
                    activity!!.runOnUiThread {
                        addMessage(it)
                    }
                }

            }
            editMessage.text.clear()
        }
    }

    private fun setSmallLayout() {
        activity!!.runOnUiThread {
            smallLayout.removeAllViews()
        }
    }

    fun expandLayout() {
        smallScroll?.visibility = ViewGroup.GONE
        messageLayout?.visibility = ViewGroup.VISIBLE
    }

    fun reduceLayout() {
        smallScroll?.visibility = ViewGroup.VISIBLE
        messageLayout?.visibility = ViewGroup.GONE
    }

    fun addMessage(messageVO: MessageVO) {
        messageRecyclerAdapter.addNewMessage(messageVO)
        recyclerMessage.scrollToPosition(0)
    }
}
