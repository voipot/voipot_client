package voipot.jnu.conoz.com.voipotclient.adapter

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import voipot.jnu.conoz.com.voipotclient.CommonApp
import voipot.jnu.conoz.com.voipotclient.PullImgActivity
import voipot.jnu.conoz.com.voipotclient.R
import voipot.jnu.conoz.com.voipotclient.service.ACTION_RECV_IMG
import voipot.jnu.conoz.com.voipotclient.service.ImgRecvService
import voipot.jnu.conoz.com.voipotclient.service.KEY_FILE_PATH
import voipot.jnu.conoz.com.voipotclient.service.KEY_IMG_RECV_RESULT
import voipot.jnu.conoz.com.voipotclient.transport.RequestImgTCP
import voipot.jnu.conoz.com.voipotclient.util.ScheduleDbHelper
import java.io.File

class ImgPagerAdapter(imgsList: MutableList<String>?, private val voId: Long) : PagerAdapter() {
    private val imgsList = ArrayList<String>(imgsList)

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imgItemWidth = container.measuredWidth / 5 - 8
        val layout = LayoutInflater.from(container.context)
                .inflate(R.layout.item_img_pager, container, false) as LinearLayout

        if (imgsList.size != 0) {
            for (i in position * 5..position * 5 + 4) {
                if (i < imgsList.size) {
                    ImageView(container.context).apply {
                        layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT).apply {
                            scaleType = ImageView.ScaleType.FIT_XY
                            setPadding(2, 2, 2, 2)
                        }

                    }.let { imgView ->
                        if (imgsList[i] != "x") {
                            println("img : ${imgsList[i]}")
                            Glide.with(container.context).load(CommonApp.imgDirectory + File(imgsList[i])).thumbnail(0.05f).into(imgView)
                            imgView.setOnClickListener {
                                Intent(container.context, PullImgActivity::class.java).apply {
                                    putExtra("imgPath", imgsList[i])
                                }.let { intent ->
                                    container.context.startActivity(intent)
                                }
                            }
                        } else {
                            imgView.setOnClickListener {
                                val progress = container.context.indeterminateProgressDialog(message = "이미지 다운로드 중!")
                                progress.setCancelable(false)
                                progress.show()

                                RequestImgTCP().request(voId, i) { returnedMsg ->
                                    if (returnedMsg == "fail"){
                                        progress.dismiss()
                                        container.context.runOnUiThread {
                                            Toast.makeText(container.context, "가이드와의 접속상태가 불량합니다", Toast.LENGTH_LONG).show()
                                        }
                                    }
                                    else if (returnedMsg != null && returnedMsg.take(4) == "send") {
                                        val fileName = "/id${voId}po$i." + returnedMsg.drop(5)
                                        Intent(container.context, ImgRecvService::class.java).apply {
                                            putExtra(KEY_FILE_PATH, CommonApp.imgDirectory + fileName)
                                        }.let { intent ->
                                            container.context.startService(intent)
                                        }

                                        LocalBroadcastManager.getInstance(container.context).registerReceiver(object : BroadcastReceiver() {
                                            override fun onReceive(context: Context?, intent: Intent?) {
                                                println("img 저장 안됩니까")
                                                if (intent!!.getBooleanExtra(KEY_IMG_RECV_RESULT, false)) {
                                                    imgsList[i] = fileName
                                                    val joinedImgString = imgsList.joinToString(",")
                                                    println("img 저장 됩니다")
                                                    ScheduleDbHelper.getInstance(container.context, ScheduleDbHelper.DBName.DB_GUIDE_SCHEDULE)
                                                            .updateImgs(voId, joinedImgString)
                                                    Glide.with(container.context).load(CommonApp.imgDirectory + File(imgsList[i]))
                                                            .thumbnail(0.01f).into(imgView)
                                                }
                                                progress.dismiss()
                                            }
                                        }, IntentFilter(ACTION_RECV_IMG))
                                    }
                                }
                            }
                        }

                        val ll = LinearLayout(container.context).apply {
                            layoutParams = LinearLayout.LayoutParams(imgItemWidth, imgItemWidth).apply {
                                setMargins(4, 2, 4, 2)
                            }
                            setBackgroundResource(R.drawable.edge_rect_mint)
                            addView(imgView)
                        }
                        layout.addView(ll)
                    }
                } else {
                    break
                }
            }
        }

        container.addView(layout)

        return layout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return if (imgsList.size % 5 == 0) {
            imgsList.size / 5
        } else {
            imgsList.size / 5 + 1
        }
    }
}