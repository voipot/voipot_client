package voipot.jnu.conoz.com.voipotclient.adapter.schedule

import android.content.Context
import android.support.v7.widget.RecyclerView
import org.jetbrains.anko.runOnUiThread
import voipot.jnu.conoz.com.voipotclient.vo.ScheduleVO

abstract class ScheduleRecyclerAdapter<T : RecyclerView.ViewHolder>(val context: Context, scheduleData: List<ScheduleVO>) : RecyclerView.Adapter<T>() {
    open var scheduleData: ArrayList<ScheduleVO> = ArrayList(scheduleData)
    protected var clickable = true

    override fun getItemCount(): Int {
        println("scheduleData.size : ${scheduleData.size}")
        return scheduleData.size
    }

    fun add(scheduleVO: ScheduleVO, position: Int){
        println("schedule add ${scheduleVO.title} position : $position")
        scheduleData.add(position, scheduleVO)
        notifyItemInserted(position)
        delayedNotifyDataSetChanged()
    }

    fun addAll(scheduleData: List<ScheduleVO>) {
        this.scheduleData = ArrayList(scheduleData)
        println("스케줄 갱신 완료")
        notifyDataSetChanged()
    }

    fun getAllData(): ArrayList<ScheduleVO> {
        return ArrayList(scheduleData)
    }

    //애니메이션 효과후 실행하기위해서
    fun delayedNotifyDataSetChanged(){
        Thread{
            Thread.sleep(500)
            context.runOnUiThread {
                notifyDataSetChanged()
                clickable = true
            }
        }.start()
    }
}