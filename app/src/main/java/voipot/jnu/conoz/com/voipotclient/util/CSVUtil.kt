//package voipot.jnu.conoz.com.voipotclient.util
//
//import android.util.Log
//import com.opencsv.CSVReader
//import com.opencsv.CSVWriter
//import java.io.*
//import java.nio.charset.StandardCharsets
//
//class CSVUtil(private var messagePath: String) {
//
//    fun addWrite(list: Array<String>) {
//        val csvWriter = CSVWriter(OutputStreamWriter(FileOutputStream(messagePath, true), StandardCharsets.UTF_8))
//        try {
//            csvWriter.writeNext(list)
//        } catch (e: IOException) {
//            Log.e("CSVUtil error", e.toString())
//        } finally {
//            csvWriter.close()
//        }
//    }
//
//    fun readAll(): ArrayList<Array<String>>? {
//        val csvReader = CSVReader(InputStreamReader(FileInputStream(messagePath), StandardCharsets.UTF_8))
//        var arrayList: ArrayList<Array<String>>? = null
//        try {
//            arrayList = ArrayList(csvReader.readAll())
//        } catch (e: IOException) {
//            Log.e("CSVUtil error", e.toString())
//        } finally  {
//            csvReader.close()
//        }
//
//        Log.e("CSVUtil", "csvReader.readAll() : ${arrayList!!.size}")
//        return arrayList
//    }
//
//    //가이드가 보낸 메시지 두개 얻어오는 메서드
//    fun guideLastRead() : ArrayList<Array<String>>? {
//        val resultList = ArrayList<Array<String>>()
//        var checker = 0
//
//        val arrayList: ArrayList<Array<String>>? = readAll()
//        for (int in arrayList!!.size - 1 downTo 0){
//            if (arrayList[int][0] == "guide"){
//                if (checker == 2){
//                    break
//                }
//                resultList.add(arrayList[int].clone())
//                checker++
//            }
//        }
//
//        return resultList
//    }
//}