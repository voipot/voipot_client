package voipot.jnu.conoz.com.voipotclient.transport

import android.media.AudioFormat
import android.media.AudioManager
import android.media.AudioTrack
import voipot.jnu.conoz.com.voipotclient.CommonApp
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket

class VoiceUDP{
    private val SAMPLE_RATE = 8000
    private val SAMPLE_INTERVAL = 20
    private val SAMPLE_SIZE = 2
    private val BUF_SIZE = SAMPLE_INTERVAL * SAMPLE_INTERVAL * SAMPLE_SIZE * 2
    private var enablePlay = false

    fun startPlay() {
        if (!enablePlay) {
            enablePlay = true
            val receiveThread = Thread(Runnable {
                println( "[startPlayOneToOne] 수신 쓰레드 시작 tid: " + Thread.currentThread().id)
                //수신한 오디오를 재생할 AudioTrack
                val track = AudioTrack(AudioManager.STREAM_MUSIC, SAMPLE_RATE, AudioFormat.CHANNEL_OUT_MONO,
                        AudioFormat.ENCODING_PCM_16BIT, BUF_SIZE, AudioTrack.MODE_STREAM)
                track.play()
                try {
                    // 오디오를 수신할 소켓 정의
                    val socket = DatagramSocket(CommonApp.PORT_VOICE)
                    val buf = ByteArray(BUF_SIZE)

                    while (enablePlay) {
                        // 패킷으로 부터 수신된 오디오를 재생
                        val packet = DatagramPacket(buf, BUF_SIZE)
                        socket.receive(packet)
                        val senderIp = packet.address.hostAddress.toString()
                        println( "sender IP : $senderIp ")
                        if (packet.address.hostAddress.toString() != CommonApp.myIP) {
                            println( "[startPlayOneToOne] Packet 수신: " + packet.length)
                            track.write(packet.data, 0, BUF_SIZE)
                        }
                    }
                    socket.disconnect()
                    socket.close()
                    track.stop()
                    track.flush()
                    track.release()
                    enablePlay = false
                } catch (e: IOException) {
                    enablePlay = false
                    e.printStackTrace()
                }
            })
            receiveThread.start()
        }
    }

    fun stopPlay(){
        enablePlay = false
    }
}