package voipot.jnu.conoz.com.voipotclient.transport

import voipot.jnu.conoz.com.voipotclient.CommonApp
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

class MessageUDP {
    fun sendMessage(message : String) {
        Thread(Runnable {
            var socket: DatagramSocket? = null
            try {
                println( "GUIDE IP : " + CommonApp.guideIP)
                val inetAddress = InetAddress.getByName(CommonApp.guideIP)
                val sendData = message.toByteArray()
                val packet = DatagramPacket(sendData, sendData.size, inetAddress, CommonApp.PORT_MESSAGE)
                socket = DatagramSocket()
                socket.broadcast = true
                socket.send(packet)
                socket.close()
                println( "UDP Broadcast Send " + String(sendData, 0, sendData.size))
            } catch (e: IOException) {
                e.printStackTrace()
                System.err.println( "IOException : " + e.message)
            } finally {
                socket?.close()
            }
        }).start()
    }
}