package voipot.jnu.conoz.com.voipotclient.fragment

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.transition.TransitionManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.orhanobut.dialogplus.DialogPlus
import com.orhanobut.dialogplus.ViewHolder
import kotlinx.android.synthetic.main.fragment_myschedule.*
import org.jetbrains.anko.support.v4.runOnUiThread
import voipot.jnu.conoz.com.voipotclient.CommonApp
import voipot.jnu.conoz.com.voipotclient.DirectoryActivity
import voipot.jnu.conoz.com.voipotclient.R
import voipot.jnu.conoz.com.voipotclient.adapter.schedule.ModifyScheduleAdapter
import voipot.jnu.conoz.com.voipotclient.adapter.schedule.ScheduleRecyclerAdapter
import voipot.jnu.conoz.com.voipotclient.adapter.schedule.TextScheduleAdapter
import voipot.jnu.conoz.com.voipotclient.service.*
import voipot.jnu.conoz.com.voipotclient.util.ExcelUtil
import voipot.jnu.conoz.com.voipotclient.util.ScheduleDbHelper
import voipot.jnu.conoz.com.voipotclient.vo.ScheduleVO

const val SCHEDULE_REQUEST_CODE = 11
const val KEY_IS_GUIDE = "KEY_IS_GUIDE"

class ScheduleFragment : Fragment() {
    private lateinit var adapter: ScheduleRecyclerAdapter<out RecyclerView.ViewHolder>
    private lateinit var writeClickListener: OnWriteClickListener
    private lateinit var scheduleDbHelper: ScheduleDbHelper
    private var isGuide = false

    private val excelUtil: ExcelUtil by lazy {
        ExcelUtil()
    }

    interface OnWriteClickListener {
        fun onWriteClick()
    }

    companion object {
        @JvmStatic
        fun newClientInstance(listener: OnWriteClickListener) =
                ScheduleFragment().apply {
                    arguments = Bundle().apply {
                        putBoolean(KEY_IS_GUIDE, false)
                    }
                    writeClickListener = listener
                }

        @JvmStatic
        fun newGuideInstance() =
                ScheduleFragment().apply {
                    arguments = Bundle().apply {
                        putBoolean(KEY_IS_GUIDE, true)
                    }
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isGuide = it.getBoolean(KEY_IS_GUIDE)
        }

        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                ScheduleVO().apply {
                    id = intent.getLongExtra(KEY_ID, 0)
                    time = intent.getStringExtra(KEY_TIME)
                    title = intent.getStringExtra(KEY_TITLE)
                    memo = intent.getStringExtra(KEY_MEMO)
                    setBlankImgList(intent.getIntExtra(KEY_IMG_SIZE, 0))
                }.let {
                    it.id = scheduleDbHelper.add(it)
                    runOnUiThread {
                        adapter.add(it, scheduleDbHelper.getPosition(it))
                    }
                }
            }
        }

        scheduleDbHelper = if (isGuide) {
            LocalBroadcastManager.getInstance(activity!!).registerReceiver(receiver, IntentFilter(ACTION_RECV_SCHEDULE_RESULT))
            ScheduleDbHelper.getInstance(activity!!, ScheduleDbHelper.DBName.DB_GUIDE_SCHEDULE)
        } else {
            ScheduleDbHelper.getInstance(activity!!, ScheduleDbHelper.DBName.DB_MY_SCHEDULE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_myschedule, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecyclerView()
        initView()
    }

    private fun initRecyclerView() {
        scheduleRecycler.layoutManager = LinearLayoutManager(activity!!, LinearLayout.VERTICAL, false)
        TextScheduleAdapter(activity!!, arrayListOf()).apply {
            addAll(scheduleDbHelper.getAll())
        }.also {
            scheduleRecycler.adapter = it
            adapter = it
        }
    }

    private fun initView() {
        if (isGuide) {
            bottom.visibility = ViewGroup.GONE
        } else {
            btnWrite.setOnClickListener {
                writeClickListener.onWriteClick()
                beginRevise()
            }
            btnOpen.setOnClickListener {
                Intent(activity!!, DirectoryActivity::class.java).let { intent ->
                    activity!!.startActivityFromFragment(this@ScheduleFragment, intent, SCHEDULE_REQUEST_CODE)
                }
            }
            btnAddSchedule.setOnClickListener {
                DialogPlus.newDialog(activity).apply {
                    setContentHolder(ViewHolder(R.layout.dialog_wirte_schedule))

                    isExpanded = true
                    isCancelable = true
                    setGravity(Gravity.BOTTOM)
                    setOnClickListener { dialog, view ->
                        when (view.id) {
                            R.id.btnDone -> {
                                val vo = ScheduleVO().apply {
                                    time = (dialog.findViewById(R.id.txtTime2) as TextView).text.toString()
                                    title = (dialog.findViewById(R.id.editTitle) as EditText).text.toString()
                                    memo = (dialog.findViewById(R.id.editMemo) as EditText).text.toString()
                                }

                                with(scheduleDbHelper) {
                                    vo.id = add(vo)

                                    TransitionManager.beginDelayedTransition(scheduleRecycler)
                                    this@ScheduleFragment.adapter.add(vo, getPosition(vo))
                                }

                                dialog.dismiss()
                            }

                            R.id.btnDown -> {
                                dialog.dismiss()
                            }
                        }
                    }
                    contentBackgroundResource = android.R.color.transparent
                    isExpanded = false
                }.create().show()
            }
        }

        getCurrentSchedule()?.run {
            txtTime.text = time
            txtTitle.text = title
            txtMemo.text = memo
        }
    }

    private fun getCurrentSchedule(): ScheduleVO? {
        val curTime = CommonApp.getCurrentTime().takeLast(5).split(":").let {
            it[0].toInt() * 60 + it[1].toInt()
        }

        adapter.getAllData().run {
            if (isEmpty()) {
                return null
            }

            forEach { scheduleVO ->
                scheduleVO.time.split(":").let {
                    if (curTime - it[0].toInt() * 60 + it[1].toInt() < 0) {
                        return scheduleVO
                    }
                }
            }
            return last()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SCHEDULE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val filePath: String?

            if (data!!.hasExtra("fileName")) {
                filePath = data.getStringExtra("fileName")

                excelUtil.fileOpen(filePath!!)
                with(scheduleDbHelper) {
                    overwrite(excelUtil.resultVOList)
                    adapter.addAll(getAll())
                }
            }
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
    }

    fun reduceLayout() {
        smallLayout?.visibility = View.VISIBLE
        scheduleRecycler?.visibility = View.GONE
    }

    fun expandLayout() {
        smallLayout?.visibility = View.GONE
        scheduleRecycler?.visibility = View.VISIBLE
    }

    private fun beginRevise() {
        scheduleRecycler.adapter = ModifyScheduleAdapter(activity!!, adapter.getAllData()).also { adapter ->
            this@ScheduleFragment.adapter = adapter
        }
        btnAddSchedule.visibility = ViewGroup.VISIBLE
        writeLayout.visibility = ViewGroup.GONE
    }

    fun endRevise() {
        scheduleRecycler.adapter = TextScheduleAdapter(activity!!, adapter.getAllData()).also { adapter ->
            this@ScheduleFragment.adapter = adapter
        }
        btnAddSchedule.visibility = ViewGroup.GONE
        writeLayout.visibility = ViewGroup.VISIBLE
    }
}