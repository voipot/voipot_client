package voipot.jnu.conoz.com.voipotclient.service

import android.app.IntentService
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import voipot.jnu.conoz.com.voipotclient.CommonApp
import java.io.*
import java.net.ServerSocket

const val ACTION_RECV_SCHEDULE_RESULT = "ACTION_RECV_SCHEDULE_RESULT"
const val KEY_ID = "KEY_ID"
const val KEY_TIME = "KEY_TIME"
const val KEY_TITLE = "KEY_TITLE"
const val KEY_MEMO = "KEY_MEMO"
const val KEY_IMG_SIZE = "KEY_IMG_SIZE"

class RecvScheduleService : IntentService("RecvScheduleService") {
    private var isOn = true
    override fun onHandleIntent(intent: Intent?) {
        var serverSocket: ServerSocket? = null

        try {
            println("start recv schedule")
            serverSocket = ServerSocket(CommonApp.PORT_SCHEDULE)

            while (true) {
                val socket = serverSocket.accept()
                println("recv socket")

                try {
                    val bufferReader = BufferedReader(InputStreamReader(socket.getInputStream()))
                    val readLine = bufferReader.readLine()

                    if (readLine.isNotEmpty()) {
                        println("recv data = $readLine")
                        readLine.split("|,").let {
                            Intent(ACTION_RECV_SCHEDULE_RESULT).apply {
                                putExtra(KEY_ID, it[0].toLong())
                                putExtra(KEY_TIME, it[1])
                                putExtra(KEY_TITLE, it[2])
                                putExtra(KEY_MEMO, it[3])
                                putExtra(KEY_IMG_SIZE, it[4].toInt())
                            }.let { intent ->
                                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
                            }
                        }

                        val printWriter = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)

                        printWriter.println("complete")
                        printWriter.flush()
                    }

                } catch (t: Throwable) {
                    t.printStackTrace()
                    Log.e(this.toString(), "Exception: " + t.message)
                } finally {
                    socket.close()
                }
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            Log.e(this.toString(), "Exception: " + e.message)
        } catch (e: InterruptedException) {
            e.printStackTrace()
            Log.e(this.toString(), "Exception: " + e.message)
        } finally {
            serverSocket?.close()
        }
    }

    override fun onDestroy() {
        isOn = false
        super.onDestroy()
    }
}
