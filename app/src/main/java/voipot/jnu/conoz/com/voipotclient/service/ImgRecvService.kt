package voipot.jnu.conoz.com.voipotclient.service

import android.app.IntentService
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import java.io.*
import java.lang.NumberFormatException
import java.net.ServerSocket
import java.net.Socket

const val ACTION_RECV_IMG = "ACTION_RECV_IMG"
const val KEY_FILE_PATH = "KEY_FILE_PATH"
const val KEY_IMG_RECV_RESULT = "KEY_IMG_RECV_RESULT"

class ImgRecvService : IntentService("ImgRecvService") {
    override fun onHandleIntent(intent: Intent?) {
        val filePath = intent!!.getStringExtra(KEY_FILE_PATH)
        println("img Path : $filePath")
        val f = File(filePath)
        if (f.exists()) {
            println("있당께")
        } else {
            println("없당께")
        }
        println("img recv start")

        val server = ServerSocket(8899)
        server.soTimeout = 10000
        val fos = FileOutputStream(f)
        var socket: Socket? = null
        var inputStream: InputStream? = null
        try {
            println("img recv 대기중")
            socket = server.accept()
            inputStream = socket.getInputStream()

            var buf = ByteArray(128)
            inputStream.read(buf, 0, buf.size)

            println("img img size ${String(buf)} size = ${String(buf).length}")

            var fileSize = 0L
            synchronized(buf) {
                //채워지지 않은 배열 지우기
                for ((i, bb) in buf.withIndex()) {
                    if (bb == 0.toByte()) {
                        ByteArray(i).apply {
                            for (k in 0..(i - 1)) {
                                set(k, buf[k])
                            }
                        }.let {
                            buf = it
                        }
                        break
                    }
                }

                val droped = String(buf).drop(8).trim()

                try{
                    fileSize = droped.toLong()
                } catch (e: NumberFormatException){
                    println("size : ${droped.length}")
                }
            }

            inputStream = socket.getInputStream()
            println("img recv!")
            val buffer = ByteArray(DEFAULT_BUFFER_SIZE)

            while (fileSize >= 0) {
                val readBytes = inputStream.read(buffer, 0, buffer.size)
                fileSize -= readBytes
                fos.write(buffer, 0, readBytes)
            }

            println("img 끝?")
            LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(ACTION_RECV_IMG).putExtra(KEY_IMG_RECV_RESULT, true))

            val out = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)
            out.println("img transfer complete")
            println("img transfer completed.")
        } catch (e: IOException) {
            LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(ACTION_RECV_IMG).putExtra(KEY_IMG_RECV_RESULT, false))
            e.printStackTrace()
        } finally {
            server.close()
            fos.close()
            socket?.close()
            inputStream?.close()
        }
    }

    /*fun recv(filePath: String) {
        Thread(Runnable {
            println("img Path : $filePath")
            val f = File(filePath)
            if (f.exists()) {
                println("있당께")
            } else {
                println("없당께")
            }
            println("img recv start")
            val server = ServerSocket(8899)
            server.soTimeout = 10000
            val fos = FileOutputStream(f)
            var socket: Socket? = null
            var inputStream: InputStream? = null
            try {
                println("img recv 대기중")
                socket = server.accept()
                inputStream = socket.getInputStream()

                var buf = ByteArray(128)
                inputStream.read(buf, 0, buf.size)

                println("img img size ${String(buf)}")

                var fileSize = 0L
                synchronized(buf) {
                    //채워지지 않은 배열 지우기
                    for ((i, bb) in buf.withIndex()) {
                        if (bb == 0.toByte()) {
                            ByteArray(i).apply {
                                for (k in 0..(i - 1)) {
                                    set(k, buf[k])
                                }
                            }.let {
                                buf = it
                            }
                            break
                        }
                    }
                    fileSize = String(buf).drop(8).trim().toLong()
                }

                inputStream = socket.getInputStream()
                println("img recv!")
                val buffer = ByteArray(DEFAULT_BUFFER_SIZE)

                while (fileSize >= 0) {
                    val readBytes = inputStream.read(buffer, 0, buffer.size)
                    fileSize -= readBytes
                    fos.write(buffer, 0, readBytes)
                }

                println("img 끝?")

                val out = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)
                out.println("img transfer complete")
                println("img transfer completed.")
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                server.close()
                fos.close()
                socket?.close()
                inputStream?.close()
            }
        }).start()
    }*/
}