package voipot.jnu.conoz.com.voipotclient.adapter.schedule

import android.content.Context
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.transition.Slide
import android.transition.TransitionManager
import android.view.*
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.item_modifiable_schedule.view.*
import kotlinx.android.synthetic.main.view_schedule_delete.view.*
import voipot.jnu.conoz.com.voipotclient.R
import voipot.jnu.conoz.com.voipotclient.util.ScheduleDbHelper
import voipot.jnu.conoz.com.voipotclient.vo.ScheduleVO

class ModifyScheduleAdapter(context: Context, scheduleData: List<ScheduleVO>) :
        ScheduleRecyclerAdapter<ModifyScheduleAdapter.ModifiableHolder>(context, scheduleData) {
    private val dbHelper: ScheduleDbHelper by lazy {
        ScheduleDbHelper.getInstance(context, ScheduleDbHelper.DBName.DB_MY_SCHEDULE)
    }
    private val imm by lazy {
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModifiableHolder {
        return ModifiableHolder(LayoutInflater.from(context).inflate(R.layout.item_modifiable_schedule, parent, false))
    }

    override fun onBindViewHolder(holder: ModifiableHolder, position: Int) {
        holder.apply {
            time.text = scheduleData[position].time
            txtTitle.text = scheduleData[position].title
            txtMemo.text = scheduleData[position].memo

            time.text = scheduleData[position].time
            editTitle.setText(scheduleData[position].title)
            editMemo.setText(scheduleData[position].memo)

            txtDone.setOnClickListener {
                TransitionManager.beginDelayedTransition(itemView.parentLayout)
                writeLayout.visibility = ViewGroup.GONE
                modifiableLayout.visibility = ViewGroup.VISIBLE

                scheduleData[position].time = time.text.toString()
                scheduleData[position].title = editTitle.text.toString()
                scheduleData[position].memo = editMemo.text.toString()
                notifyItemChanged(position)

                imm.hideSoftInputFromWindow(editMemo.windowToken, 0)

                dbHelper.update(scheduleData[position])
            }

            btnTrashCan.setOnClickListener {
                fun slideRight(holder: ModifiableHolder) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        TransitionManager.beginDelayedTransition(holder.itemView.parentLayout, Slide(Gravity.END))
                    } else {
                        TransitionManager.beginDelayedTransition(holder.itemView.parentLayout)
                    }
                }

                LayoutInflater.from(context)
                        .inflate(R.layout.view_schedule_delete, holder.itemView.parentLayout, false)
                        .apply {
                            layoutParams.height = holder.itemView.parentLayout.height

                            //뒤에 중첩되 있는 뷰(펜, 휴지통)의 클릭을 막기위해
                            setOnTouchListener { _: View, _: MotionEvent ->
                                true
                            }

                            btnYes.setOnClickListener {
                                if (clickable) {
                                    dbHelper.delete(scheduleData[position].id!!)
                                    scheduleData.removeAt(position)

                                    clickable = false

                                    TransitionManager.beginDelayedTransition(itemView.parentLayout)
                                    holder.itemView.parentLayout.removeView(this)
                                    notifyItemRemoved(position)

                                    delayedNotifyDataSetChanged()
                                }
                            }
                            btnNo.setOnClickListener {
                                slideRight(holder)
                                holder.itemView.parentLayout.removeView(this)
                            }
                        }
                        .run {
                            slideRight(holder)
                            holder.itemView.parentLayout.addView(this)
                        }
            }
        }
    }


    class ModifiableHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val modifiableLayout = itemView.modifiableLayout!!
        val time = itemView.txtTime!!
        val txtTitle = itemView.txtTitle!!
        val txtMemo = itemView.txtMemo!!
        val btnTrashCan = itemView.btnTrashCan!!
        val btnWrite = itemView.btnModify!!.apply {
            setOnClickListener {
                TransitionManager.beginDelayedTransition(itemView.parentLayout)
                itemView.modifiableLayout.visibility = ViewGroup.GONE
                itemView.writeLayout.visibility = ViewGroup.VISIBLE
            }
        }

        val writeLayout = itemView.writeLayout!!
        val editTitle = itemView.editTitle!!
        val editMemo = itemView.editMemo!!
        val txtDone = itemView.txtDone!!
    }
}