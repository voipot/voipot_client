package voipot.jnu.conoz.com.voipotclient

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.DisplayMetrics
import java.lang.ref.WeakReference
import android.view.WindowManager

class SplashActivity : AppCompatActivity() {
    private val handler = SplashActivityHandler(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_splash)

        initDensity()
        handler.sendEmptyMessageDelayed(0, 3000)

    }

    private fun initDensity() {
        val metrics = DisplayMetrics()
        this.windowManager.defaultDisplay.getMetrics(metrics)
        println("device dpi => ${metrics.densityDpi}")
        when {
            metrics.densityDpi <= 160 -> // mdpi
                CommonApp.density = 0.28f
            metrics.densityDpi <= 240 -> // hdpi
                CommonApp.density = 0.42f
            metrics.densityDpi <= 320 -> // xhdpi
                CommonApp.density = 0.57f
            metrics.densityDpi <= 480 -> // xxhdpi
                CommonApp.density = 0.85f
            metrics.densityDpi <= 560 -> // xxhdpi
                CommonApp.density = 1.0f
            metrics.densityDpi <= 640 -> // xxxhdpi
                CommonApp.density = 1.14f
        }
    }

    private class SplashActivityHandler (activity: SplashActivity) : Handler() {
        private val mActivity: WeakReference<SplashActivity> = WeakReference(activity)

        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            val i = Intent(mActivity.get(), MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mActivity.get()!!.startActivity(i)
        }
    }
}
