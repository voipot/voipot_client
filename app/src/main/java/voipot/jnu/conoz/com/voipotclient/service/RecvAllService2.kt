package voipot.jnu.conoz.com.voipotclient.service

import android.app.IntentService
import android.content.Intent
import android.util.Log
import voipot.jnu.conoz.com.voipotclient.CommonApp
import java.io.*
import java.net.ServerSocket

//const val ACTION_RECVMESSAGE_RESULT = "ACTION_RECVMESSAGE_RESULT"
//const val KEY_NAME_RESULT = "KEY_NAME_RESULT"
//const val KEY_TIME_RESULT = "KEY_TIME_RESULT"
//const val KEY_MESSAGE_RESULT = "KEY_MESSAGE_RESULT"

class RecvAllService2 : IntentService("RecvAllService") {
    private var isOn = true
    override fun onHandleIntent(intent: Intent?) {
        var serverSocket: ServerSocket? = null

        try {
            serverSocket = ServerSocket(CommonApp.PORT_ALL_RECV)
//            var datagramPacket: DatagramPacket
            while (isOn) {
                val socket = serverSocket.accept()

                try {
                    val bufferReader = BufferedReader(InputStreamReader(socket.getInputStream()))
                    val readLine = bufferReader.readLine()

                    if (readLine.isNotEmpty()) {
                        println("data = $readLine")
                    }
                    val printWriter = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)

                    printWriter.println("complete")
                    printWriter.flush()
                } catch (t: Throwable) {
                    t.printStackTrace()
                    Log.e(this.toString(), "Exception: " + t.message)
                } finally {
                    socket.close()
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
            Log.e(this.toString(), "Exception: " + e.message)
        } catch (e: InterruptedException) {
            e.printStackTrace()
            Log.e(this.toString(), "Exception: " + e.message)
        } finally {
            serverSocket?.close()
        }
    }

    override fun onDestroy() {
        isOn = false
        super.onDestroy()
    }
}
