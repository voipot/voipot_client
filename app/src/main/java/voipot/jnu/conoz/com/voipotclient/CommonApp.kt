package voipot.jnu.conoz.com.voipotclient


import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.wifi.WifiManager
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import com.rd.utils.DensityUtils
import voipot.jnu.conoz.com.voipotclient.service.RecvMessageService
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class CommonApp : Application() {
    companion object {
        const val PORT_ALL_RECV = 8887
        const val PORT_FIND_MEMBER = 8888
        const val PORT_VOICE = 8889
        const val PORT_GPS = 8890
        const val PORT_MESSAGE = 8891
        const val PORT_SCHEDULE = 8892
        const val PORT_IMG_REQUEST = 8893
        const val PORT_IMG = 8894
        const val PORT_ALL_RECV2 = 8894

        lateinit var imgDirectory: String
            private set
        var userName: String = "고객"
        var myIP: String? = null
        var guideIP: String? = null
            private set
        var guideID: String? = null
        var guidePW: String? = null
        var density: Float = 1f

        @SuppressLint("SimpleDateFormat")
        fun getCurrentTime(): String {
            return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date(System.currentTimeMillis()))
        }
    }

    fun setGuideIP() {
        val wifiManager = getSystemService(AppCompatActivity.WIFI_SERVICE) as WifiManager
        val wifiInfo = wifiManager.connectionInfo
        val ipAddress = wifiInfo.ipAddress
        val addr1 = ipAddress and 0xff
        val addr2 = ipAddress shr 8 and 0xff
        val addr3 = ipAddress shr 16 and 0xff
        val addr4 = 1

        guideIP = String.format("%d.%d.%d.%d", addr1, addr2, addr3, addr4)
    }

    private var recvMessageService: Intent? = null
    private lateinit var settingPreference: SharedPreferences

    override fun onCreate() {
        super.onCreate()

        settingPreference = getSharedPreferences("setting", Context.MODE_PRIVATE)
        isTraveling = settingPreference.getBoolean("isTraveling", false)
        userName = settingPreference.getString("clientName", "고객")
        guideIP = settingPreference.getString("guideIP", null)
        guideID = settingPreference.getString("guideID", null)
        guidePW = settingPreference.getString("guidePW", null)
        myIP = settingPreference.getString("myIP", null)

        imgDirectory = applicationContext.getExternalFilesDir(null).absolutePath + "/img"
        File(imgDirectory).mkdir()
        println("imgdirectory : $imgDirectory")

    }

    override fun onTerminate() {
        recvMessageService?.run { stopService(this) }
        super.onTerminate()
    }

    fun travelingOnOff(isTraveling: Boolean) {
        this.isTraveling = isTraveling
        val editor = settingPreference.edit()

        editor.putBoolean("isTraveling", isTraveling).apply()

        if (isTraveling) {
            editor.apply {
                putString("clientName", userName)
                putString("guideID", guideID)
                putString("guidePW", guidePW)
                putString("guideIP", guideIP)
                putString("myIP", myIP)
            }.apply()
        } else {
            editor.apply {
                putString("clientName", "")
                putString("guideID", null)
                putString("guidePW", null)
                putString("guideIP", null)
                putString("myIP", null)
            }.apply()
        }
    }

    fun startIntentService() {
        recvMessageService = Intent(this, RecvMessageService::class.java)
        startService(recvMessageService)
    }

    var isTraveling = false
}