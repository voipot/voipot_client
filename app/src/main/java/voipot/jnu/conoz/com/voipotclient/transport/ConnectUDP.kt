package voipot.jnu.conoz.com.voipotclient.transport

import voipot.jnu.conoz.com.voipotclient.CommonApp
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

class ConnectUDP {
    fun sendConn() {
        println("guideIP : " + CommonApp.guideIP)
        Thread(Runnable {
            var socket: DatagramSocket? = null
            try {
                val inetAddress = InetAddress.getByName(CommonApp.guideIP)
                val sendData = CommonApp.userName.toByteArray()
                val packet = DatagramPacket(sendData, sendData.size, inetAddress, CommonApp.PORT_FIND_MEMBER)
                socket = DatagramSocket()
                socket.broadcast = true
                socket.send(packet)

                println("UDP Send " + String(sendData, 0, sendData.size))
            } catch (e: IOException) {
                e.printStackTrace()
                System.err.println("IOException : " + e.message)
            } finally {
                socket?.close()
            }
        }).start()
    }
}