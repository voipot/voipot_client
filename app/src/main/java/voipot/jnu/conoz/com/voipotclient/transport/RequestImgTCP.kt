package voipot.jnu.conoz.com.voipotclient.transport

import android.util.Log
import voipot.jnu.conoz.com.voipotclient.CommonApp
import java.io.*
import java.net.InetAddress
import java.net.Socket

class RequestImgTCP {
    private val TAG = "RequestImgTCP"
    fun request(voId: Long, imgPosition: Int, startRecv: (returnMsg: String?) -> Unit) {
        Thread(Runnable {
            var socket: Socket? = null

            try {
                val inetAddress = InetAddress.getByName(CommonApp.guideIP)
                socket = Socket(inetAddress, 8888)
                val out = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)
                out.println("request img|,$voId|,$imgPosition")

                BufferedReader(InputStreamReader(socket.getInputStream())).let {
                    it.readLine()?.let { readLine ->
                        println("returnedMsg : $readLine")
                        startRecv(readLine)
                        out.println("start")
                    }
                }
            } catch (e: Throwable) {
                e.printStackTrace()
                startRecv("fail")
                Log.e(TAG, "IOException : " + e.message)
            } finally {
                socket?.close()
            }
        }).start()
    }
}