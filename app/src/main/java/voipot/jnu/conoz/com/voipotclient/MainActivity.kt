package voipot.jnu.conoz.com.voipotclient

import android.Manifest
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.transition.TransitionManager
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.rd.animation.type.AnimationType
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.include_main_center.*
import voipot.jnu.conoz.com.voipotclient.adapter.MyPagerAdapter
import voipot.jnu.conoz.com.voipotclient.fragment.LoginFragment
import voipot.jnu.conoz.com.voipotclient.fragment.MessageFragment
import voipot.jnu.conoz.com.voipotclient.fragment.ScheduleFragment
import voipot.jnu.conoz.com.voipotclient.service.*
import voipot.jnu.conoz.com.voipotclient.transport.GpsUDP
import voipot.jnu.conoz.com.voipotclient.transport.VoiceUDP
import voipot.jnu.conoz.com.voipotclient.util.MessageDbHelper
import voipot.jnu.conoz.com.voipotclient.util.WifiUtil
import voipot.jnu.conoz.com.voipotclient.vo.MessageVO
import java.util.*

//북마크 테스트 완료후 지울 것
class MainActivity : AppCompatActivity(), LoginFragment.OnWifiLoginListener {
    private var isListen = false
    private var isExpand = false
    private var isWifiConn = false
    private var messageFragment: MessageFragment? = null
    private var guideScheduleFragment: ScheduleFragment? = null
    private lateinit var myScheduleFragment: ScheduleFragment
    private lateinit var myPagerAdapter: MyPagerAdapter
    private lateinit var messageDbHelper: MessageDbHelper

    private val locationManager: LocationManager by lazy {
        getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    private val voiceUDP: VoiceUDP by lazy {
        VoiceUDP()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = window.decorView
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (view != null) {
                view.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        } else if (Build.VERSION.SDK_INT >= 21) {
            window.statusBarColor = Color.BLACK
        }

        setContentView(R.layout.activity_main)

        val serviceTest = Intent(this, RecvAllService2::class.java)
        startService(serviceTest)

        val recvScheduleService = Intent(this, RecvScheduleService::class.java)
        startService(recvScheduleService)

        initPermission()
        connCheck()
        initView()
        initViewPager()
        initBtnListener()
    }

    private fun initView() {
        fun adjustParams(view: View) {
            view.layoutParams.apply {
                if (width != ViewGroup.LayoutParams.MATCH_PARENT && width != ViewGroup.LayoutParams.WRAP_CONTENT) {
                    width = (width * CommonApp.density).toInt()
                }
                height = (height * CommonApp.density).toInt()
            }
        }

        adjustParams(center)
        adjustParams(bottomLayout)
        adjustParams(btnMessage)
        adjustParams(topInBottom)
        adjustParams(txtOnOff)
        (durationLayout.layoutParams as RelativeLayout.LayoutParams).apply {
            setMargins(0, 0, 0, (bottomMargin * CommonApp.density).toInt())
        }
    }

    @SuppressLint("NewApi")
    private fun initPermission() {
        fun checkPermission(permission: String) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                TedPermission.with(this)
                        .setPermissionListener(object : PermissionListener {
                            override fun onPermissionGranted() {
                                Toast.makeText(this@MainActivity, "$permission Granted", Toast.LENGTH_SHORT).show()
                            }

                            override fun onPermissionDenied(deniedPermissions: ArrayList<String>?) {
                                Toast.makeText(this@MainActivity, "$permission Denied", Toast.LENGTH_SHORT).show()
                            }
                        })
                        .setPermissions(permission)
                        .check()
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission(Manifest.permission.RECORD_AUDIO)
            checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
    }

    private fun connCheck() {
        if ((application as CommonApp).isTraveling) {
            durationLayout.visibility = ViewGroup.VISIBLE
            msgBroadcastReceiverStart()

            startService(Intent(this, WifiCheckService::class.java))
            LocalBroadcastManager.getInstance(this).registerReceiver(object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent?) {
                    isWifiConn = intent!!.getBooleanExtra(KEY_WIFI_STATE, false)

                    if (!isWifiConn) {
                        println("wifi 연결필요")
                        imgDisconn.visibility = ViewGroup.VISIBLE
                        imgConn.visibility = ViewGroup.GONE
                    } else if (imgDisconn.visibility != ViewGroup.GONE) {
                        imgDisconn.visibility = ViewGroup.GONE
                        imgConn.visibility = ViewGroup.VISIBLE
                    }
                }
            }, IntentFilter(ACTION_WIFI_CHECK))
        }
    }

    private val onWriteClickListener = object : ScheduleFragment.OnWriteClickListener {
        override fun onWriteClick() {
            expandBottomLayout()
            btnScheduleBack.visibility = ViewGroup.VISIBLE
            btnExpand.visibility = ViewGroup.GONE

            viewPager.canSwipe = false
        }
    }

    private fun initViewPager() {
        pageIndicatorView.apply {
            count = 2
            setAnimationType(AnimationType.FILL)
        }
        MyPagerAdapter(supportFragmentManager).apply {
            if ((application as CommonApp).isTraveling) {
                messageFragment = MessageFragment.newInstance()

                addFragment(ScheduleFragment.newGuideInstance().also {
                    guideScheduleFragment = it
                })
                addFragment(ScheduleFragment.newClientInstance(onWriteClickListener).also {
                    myScheduleFragment = it
                })
                addFragment(messageFragment!!)

                txtRightPagerTitle.text = resources.getText(R.string.schedule)

                gpsStart()

                (application as CommonApp).startIntentService()
            } else {
                addFragment(LoginFragment.newInstance())
                addFragment(ScheduleFragment.newClientInstance(onWriteClickListener).also {
                    myScheduleFragment = it
                })
            }
        }.also {
            viewPager.adapter = it
            myPagerAdapter = it
        }

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                pageIndicatorView.selection = position
                if ((application as CommonApp).isTraveling) {
                    when (position) {
                        0 -> {
                            txtLeftPagerTitle.text = resources.getText(R.string.guide)
                            txtRightPagerTitle.text = resources.getText(R.string.schedule)
                        }
                        1 -> {
                            txtLeftPagerTitle.text = resources.getText(R.string.my)
                            txtRightPagerTitle.text = resources.getText(R.string.schedule)
                        }
                        2 -> {

                            txtLeftPagerTitle.text = resources.getText(R.string.message)
                            txtRightPagerTitle.text = ""
                        }
                    }
                } else {
                    when (position) {
                        0 -> {
                            txtLeftPagerTitle.text = resources.getText(R.string.guide)
                            txtRightPagerTitle.text = resources.getText(R.string.hotspot)
                        }
                        1 -> {
                            txtLeftPagerTitle.text = resources.getText(R.string.my)
                            txtRightPagerTitle.text = resources.getText(R.string.schedule)
                        }
                    }
                }
            }
        })

        center.setOnTouchListener { _, event ->
            viewPager.onTouchEvent(event)
        }
    }

    private fun initBtnListener() {
        imgDisconn.setOnClickListener {
            WifiUtil().connectWifi(this) { connResult ->
                isWifiConn = connResult
                if (connResult) {
                    imgConn.visibility = ViewGroup.VISIBLE
                    imgDisconn.visibility = ViewGroup.GONE
                } else {
                    Toast.makeText(this, "가이드의 와이파이에 접속하지 못했습니다!", Toast.LENGTH_SHORT).show()
                }
            }
        }

        btnVoiceOnOff.setOnClickListener {
            if (!isListen) {
                txtOnOff.text = resources.getText(R.string.on)
                voiceUDP.startPlay()
            } else {
                txtOnOff.text = resources.getText(R.string.off)
                voiceUDP.stopPlay()
            }
            isListen = !isListen
        }

        btnExpand.setOnClickListener {
            isExpand = if (isExpand) {
                reduceBottomLayout()
                false
            } else {
                expandBottomLayout()
                true
            }
        }

        btnScheduleBack.setOnClickListener {
            TransitionManager.beginDelayedTransition(bottomLayout)
            btnScheduleBack.visibility = ViewGroup.GONE
            btnExpand.visibility = ViewGroup.VISIBLE

            myScheduleFragment.endRevise()
            viewPager.canSwipe = true
        }

        btnExit.setOnClickListener {
            (application as CommonApp).travelingOnOff(false)
            durationLayout.visibility = ViewGroup.GONE

            TransitionManager.beginDelayedTransition(viewPager)
            viewPager.currentItem = 0
            myPagerAdapter.replaceFragmentList(0, LoginFragment.newInstance())
            myPagerAdapter.removeFragment(myPagerAdapter.count - 1)
        }
    }

    private fun msgBroadcastReceiverStart() {
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val recvMessage = intent.getStringExtra(KEY_MESSAGE_RESULT)
                val recvName = intent.getStringExtra(KEY_NAME_RESULT)

                if (recvMessage.isNotEmpty()) {
                    println("recvMessage : $recvMessage")
                    messageDbHelper = MessageDbHelper.getInstance(this@MainActivity)

                    MessageVO().apply {
                        name = recvName
                        message = recvMessage
                        time = CommonApp.getCurrentTime()   //요거 나중에 받을거여
                    }.let {
                        messageDbHelper.add(it)
                        runOnUiThread {
                            messageFragment?.addMessage(it)
                        }
                    }
                }
            }
        }

        val broadCastManager = LocalBroadcastManager.getInstance(this)
        broadCastManager.registerReceiver(receiver, IntentFilter(ACTION_RECVMESSAGE_RESULT))
    }

    private fun expandBottomLayout() {
        TransitionManager.beginDelayedTransition(bottomLayout)
        RelativeLayout.LayoutParams(bottomLayout.layoutParams as RelativeLayout.LayoutParams)
                .apply {
                    addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                    addRule(RelativeLayout.BELOW, R.id.pageIndicatorView)
                    setMargins(18, 45, 18, 0)
                }
                .let {
                    bottomLayout.layoutParams = it
                }

        guideScheduleFragment?.expandLayout()
        messageFragment?.expandLayout()
        myScheduleFragment.expandLayout()
    }

    private fun reduceBottomLayout() {
        TransitionManager.beginDelayedTransition(bottomLayout)
        RelativeLayout.LayoutParams(bottomLayout.layoutParams as RelativeLayout.LayoutParams)
                .apply {
                    addRule(RelativeLayout.BELOW, 0)
                }
                .let {
                    bottomLayout.layoutParams = it
                }
        guideScheduleFragment?.reduceLayout()
        messageFragment?.reduceLayout()
        myScheduleFragment.reduceLayout()
    }

    override fun onLoginComplete() {
        (application as CommonApp).apply {
            travelingOnOff(true)
        }

        val recvScheduleService = Intent(this, RecvScheduleService::class.java)
        startService(recvScheduleService)

        durationLayout.visibility = ViewGroup.VISIBLE

        myPagerAdapter.replaceFragmentList(0, ScheduleFragment.newGuideInstance().also {
            guideScheduleFragment = it
        })
        messageFragment = MessageFragment.newInstance()
        myPagerAdapter.addFragment(messageFragment!!)
        txtRightPagerTitle.text = resources.getText(R.string.schedule)

        gpsStart()
        msgBroadcastReceiverStart()
        (application as CommonApp).startIntentService()
    }

    @SuppressLint("MissingPermission")
    private fun gpsStart() {
        fun initGPS() {
            val gpsUDP = GpsUDP()
            val locationListener = object : LocationListener {
                override fun onLocationChanged(location: Location) {
                    location.let {
                        gpsUDP.sendLatLng(it.latitude, it.longitude)
                    }
                }

                override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
                override fun onProviderEnabled(provider: String) {}
                override fun onProviderDisabled(provider: String) {}
            }

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0.toFloat(), locationListener)
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0.toFloat(), locationListener)

            val locationProvider = LocationManager.GPS_PROVIDER
            locationManager.getLastKnownLocation(locationProvider)?.let {
                gpsUDP.sendLatLng(it.latitude, it.longitude)
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                TedPermission.with(this)
                        .setPermissionListener(object : PermissionListener {
                            override fun onPermissionGranted() {
                                Toast.makeText(this@MainActivity, "Permission Granted", Toast.LENGTH_SHORT).show()
                                initGPS()
                            }

                            override fun onPermissionDenied(deniedPermissions: ArrayList<String>?) {
                                Toast.makeText(this@MainActivity, "Permission Denied", Toast.LENGTH_SHORT).show()
                            }
                        })
                        .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                        .check()
            } else {
                initGPS()
            }
        } else {
            initGPS()
        }
    }
}
