package voipot.jnu.conoz.com.voipotclient.fragment


import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.transition.Slide
import android.transition.TransitionManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_login.*
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import voipot.jnu.conoz.com.voipotclient.CommonApp
import voipot.jnu.conoz.com.voipotclient.R
import voipot.jnu.conoz.com.voipotclient.util.WifiUtil

class LoginFragment : Fragment() {
    private lateinit var wifiManager: WifiManager
    private lateinit var progress: ProgressDialog
    private var onWifiLoginListener: OnWifiLoginListener? = null
    private var receiver: BroadcastReceiver? = null
    private var isLogin: Boolean = false
    private var wifiDelay = 0

    companion object {
        @JvmStatic
        fun newInstance() =
                LoginFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun onAttach(context: Context?) {
        if (context is OnWifiLoginListener)
            onWifiLoginListener = context
        else
            throw RuntimeException("OnWifiLoginListener 구현하세요..")
        super.onAttach(context)
    }

    override fun onDetach() {
        onWifiLoginListener = null
        super.onDetach()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = layoutInflater.inflate(R.layout.fragment_login, container, false);

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnNext.setOnClickListener {
            CommonApp.guideID = editGuideID.text.toString()
            CommonApp.guidePW = editGuidePW.text.toString()
            WifiUtil().connectWifi(activity as AppCompatActivity){ connResult ->
                if (connResult){
                    setNickname()
                }
            }
        }
        btnDone.setOnClickListener {
            if (!editNickname.text.isNullOrEmpty()) {
                CommonApp.userName = editNickname.text.toString()
                onWifiLoginListener!!.onLoginComplete()
            }
        }
    }

    override fun onDestroy() {
        receiver?.let {
            activity!!.unregisterReceiver(receiver)
        }
        super.onDestroy()
    }

    private fun connectWifi() {
        progress = indeterminateProgressDialog(message = "Wifi 켜는 중!!")
        progress.show()
        wifiManager = activity!!.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

        if (!wifiManager.isWifiEnabled) {
            wifiManager.isWifiEnabled = true
            progress.cancel()
            println("reConnect Wifi!")
            connectWifi()
        } else {
            println("connect Wifi!")
            val wifiConfig = WifiConfiguration()
            CommonApp.guideID = editGuideID.text.toString()
            CommonApp.guidePW = editGuidePW.text.toString()

            wifiConfig.SSID = String.format("\"%s\"", CommonApp.guideID)
            wifiConfig.preSharedKey = String.format("\"%s\"", CommonApp.guidePW)

            val netId = wifiManager.addNetwork(wifiConfig)
            println("netId : $netId")

            wifiManager.disconnect()
            if (netId == -1) {
                progress.cancel()
                println("wifi 연결실패 : $netId")
                Toast.makeText(activity!!, "아이디 또는 패스워드가 틀렸습니다.", Toast.LENGTH_SHORT).show()
            } else {
                Thread {
                    Thread.sleep(1000)
                    if (wifiManager.enableNetwork(netId, true)) {
                        println("wifi 연결도전")
                        receiver = object : BroadcastReceiver() {
                            override fun onReceive(context: Context, intent: Intent) {
                                println("receive")
                                wifiManager.connectionInfo.let {
                                    //ssid가 앞뒤로 따옴표 붙어서 나옴...
                                    if (it.ssid == "\"${CommonApp.guideID}\"" && wifiManager.connectionInfo.ipAddress != 0) {
                                        isLogin = true
                                        activity!!.runOnUiThread {
                                            progress.cancel()
                                        }
                                        println("wifi 진짜 연결성공")
                                        setNickname()
                                        (activity!!.application as CommonApp).setGuideIP()
                                    } else {
                                        println("wifi 진짜 연결실패 ${it.ssid}")
                                        println("wifi 진짜 연결실패 ${CommonApp.guideID}")
                                        //wifiDelay 다시 6초를 세서 프로그래스 종료
                                        wifiDelay = 0
                                    }
                                }
                            }
                        }
                        activity!!.registerReceiver(receiver, IntentFilter("android.net.wifi.STATE_CHANGE"))
                    }
                }.start()

                //6초 이내로 hotspot 켜지지 않으면 유효하지 않은 핫스팟
                Thread {
                    while (wifiDelay < 6) {
                        if (isLogin) {
                            break
                        }
                        Thread.sleep(1000)
                        wifiDelay++
                        println("wifiDelay : ${wifiDelay}초")
                    }
                    if (!isLogin) {
                        activity!!.runOnUiThread {
                            Toast.makeText(activity!!, "아이디 또는 패스워드가 틀렸습니다.", Toast.LENGTH_SHORT).show()
                            progress.cancel()
                            wifiDelay = 0
                        }
                    }
                }.start()
            }
        }
    }

    private fun setNickname() {
        loginLayout.visibility = View.GONE
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(fragmentLogin, Slide(Gravity.END))
            nicknameLayout.visibility = View.VISIBLE
        }
    }

    interface OnWifiLoginListener {
        fun onLoginComplete()
    }
}
