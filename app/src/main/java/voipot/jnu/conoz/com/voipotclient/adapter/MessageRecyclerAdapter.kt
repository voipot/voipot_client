package voipot.jnu.conoz.com.voipotclient.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.item_message_left.view.*
import voipot.jnu.conoz.com.voipotclient.R
import voipot.jnu.conoz.com.voipotclient.vo.MessageVO
import java.util.*

class MessageRecyclerAdapter(private var provider: LinkedList<MessageVO>?) : RecyclerView.Adapter<MessageRecyclerAdapter.MyViewHolder>() {
    override fun getItemCount(): Int {
        println("provider.size : ${provider!!.size}")
        return provider!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (provider!![position].name) {
            "나" -> 1
            else -> 2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return when (viewType) {
            1 -> {
                RightViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_message_right, parent, false))
            }
            2 -> {
                LeftViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_message_left, parent, false))
            }
            else -> throw RuntimeException("MessageRecyclerAdapter의 viewType 설정이 안 됐습니다")
        }
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindData(provider, position)
    }

    fun addAllProvider(provider: List<MessageVO>?) {
        this.provider = LinkedList(provider)
        notifyDataSetChanged()
        println("addAllProvider provider.size : ${provider!!.size}")
    }

    fun addNewMessage(messageVO: MessageVO) {
        provider!!.addFirst(messageVO)
        notifyDataSetChanged()
    }

    fun addOldMessages(messages: List<MessageVO>) {
        provider!!.addAll(messages)
        notifyDataSetChanged()
    }

    abstract class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTime = itemView.findViewById<TextView>(R.id.txtTime)!!
        val tvMessage = itemView.findViewById<TextView>(R.id.message)!!

        abstract fun bindData(provider: List<MessageVO>?, position: Int)
    }

    private class LeftViewHolder(itemView: View) : MyViewHolder(itemView) {
        private val tvName = itemView.txtName!!

        override fun bindData(provider: List<MessageVO>?, position: Int) {
            tvName.text = provider!![position].name
            tvTime.text = provider[position].time!!.split("-", " ", ":").run {
                "${get(size - 3)}:${get(size - 2)}"
            }
            tvMessage.text = provider[position].message
        }
    }

    private class RightViewHolder(itemView: View) : MyViewHolder(itemView) {
        override fun bindData(provider: List<MessageVO>?, position: Int) {
            tvTime.text = provider!![position].time!!.split("-", " ", ":").run {
                "${get(size - 3)}:${get(size - 2)}"
            }
            tvMessage.text = provider[position].message
        }
    }
}