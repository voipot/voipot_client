package voipot.jnu.conoz.com.voipotclient.adapter

import android.database.DataSetObserver
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.view.ViewGroup

class MyPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    private var replaceList: Boolean = false

    private val fragmentList : MutableList<Fragment> by lazy {
        ArrayList<Fragment>()
    }

    override fun getItem(position: Int): Fragment {
        println("getItem : $position")
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    fun addFragment(fragment: Fragment) {
        fragmentList.add(fragment)
        notifyDataSetChanged()
    }

    fun replaceFragmentList(position: Int, fragment: Fragment){
        fragmentList[position] = fragment
        println("position : ${fragmentList[position]}")
        replaceList = true
        notifyDataSetChanged()
    }
    fun removeFragment(position: Int){
        fragmentList.removeAt(position)
        replaceList = true
        notifyDataSetChanged()
    }

    override fun notifyDataSetChanged() {
        println("notify")
        super.notifyDataSetChanged()
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

}