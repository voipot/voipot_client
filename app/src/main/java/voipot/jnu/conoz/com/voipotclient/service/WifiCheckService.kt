package voipot.jnu.conoz.com.voipotclient.service

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.support.v4.content.LocalBroadcastManager
import voipot.jnu.conoz.com.voipotclient.CommonApp


const val KEY_WIFI_STATE = "KEY_WIFI_STATE"
const val ACTION_WIFI_CHECK = "KEY_WIFI_STATE"

class WifiCheckService : IntentService("WifiCheckService") {
    override fun onHandleIntent(intent: Intent?) {
        val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val connManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        while (true) {
            try {
                println("guide id : ${CommonApp.guideID} current id : ${wifiManager.connectionInfo.ssid}")
                val isGuideWifi = wifiManager.connectionInfo.ssid == "\"${CommonApp.guideID}\""

                val ni = connManager.activeNetworkInfo
                val wifiState = ((ni != null) && (ni.isConnected) && (ni.type == ConnectivityManager.TYPE_WIFI))

                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(ACTION_WIFI_CHECK).apply {
                    putExtra(KEY_WIFI_STATE, wifiState && isGuideWifi)
                })
            } catch (ignored: Throwable) {
                ignored.stackTrace
            }

            Thread.sleep(5000)
        }
    }
}
