package voipot.jnu.conoz.com.voipotclient.util

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import voipot.jnu.conoz.com.voipotclient.vo.ScheduleVO
import java.util.*
import kotlin.collections.ArrayList


class ScheduleDbHelper(context: Context, dbName: String) : SQLiteOpenHelper(context, dbName, null, DATABASE_VERSION) {
    enum class DBName(name: String) {
        DB_MY_SCHEDULE("MY_SCHEDULE"), DB_GUIDE_SCHEDULE("GUIDE_SCHEDULE");
    }

    companion object {
        const val DATABASE_VERSION: Int = 1
        const val TABLE_NAME = "Schedule"
        const val KEY_ID = "ID"
        const val KEY_TIME = "TIME"
        const val KEY_TITLE = "TITLE"
        const val KEY_MEMO = "MEMO"
        const val KEY_IMGS = "IMGS"
        private var MY_SCHEDULE_INSTANCE: ScheduleDbHelper? = null
        private var GUIDE_SCHEDULE_INSTANCE: ScheduleDbHelper? = null

        fun getInstance(context: Context, dbName: DBName): ScheduleDbHelper {
            return when (dbName) {
                DBName.DB_MY_SCHEDULE -> if (MY_SCHEDULE_INSTANCE == null) {
                    MY_SCHEDULE_INSTANCE = ScheduleDbHelper(context, dbName.name)
                    return MY_SCHEDULE_INSTANCE!!
                } else {
                    MY_SCHEDULE_INSTANCE!!
                }

                DBName.DB_GUIDE_SCHEDULE -> if (GUIDE_SCHEDULE_INSTANCE == null) {
                    GUIDE_SCHEDULE_INSTANCE = ScheduleDbHelper(context, dbName.name)
                    return GUIDE_SCHEDULE_INSTANCE!!
                } else {
                    GUIDE_SCHEDULE_INSTANCE!!
                }
            }
        }
    }

    private val mDb: SQLiteDatabase by lazy {
        writableDatabase
    }

    override fun onCreate(db: SQLiteDatabase?) {
        println("sqlite create")
        val CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_TITLE + " TEXT," +
                KEY_MEMO + " TEXT," +
                KEY_TIME + " TEXT," +
                KEY_IMGS + " TEXT);"
        db!!.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun add(scheduleVO: ScheduleVO): Long {
        return mDb.insert(TABLE_NAME, null, ContentValues().apply {
            if (scheduleVO.id != null) {
                put(KEY_ID, scheduleVO.id)
            }
            put(KEY_TITLE, scheduleVO.title)
            put(KEY_TIME, scheduleVO.time)
            put(KEY_MEMO, scheduleVO.memo)
            put(KEY_IMGS, scheduleVO.getJoinedImgString())
        })
    }

    fun delete(id: Long) {
        val query = "DELETE FROM $TABLE_NAME WHERE $KEY_ID = $id"
        mDb.execSQL(query)
    }

    fun overwrite(scheduleVOs: List<ScheduleVO>) {
        mDb.execSQL("DELETE FROM $TABLE_NAME")

        scheduleVOs.forEach {
            add(it)
        }
    }

    fun updateImgs(id: Long, joinedImgString: String) {
        val query = "UPDATE $TABLE_NAME SET" +
                " $KEY_IMGS = '$joinedImgString}'" +
                " WHERE $KEY_ID = $id;"
        mDb.execSQL(query)
    }

    fun update(scheduleVO: ScheduleVO) {
        val query = "UPDATE $TABLE_NAME SET" +
                " $KEY_TITLE = '${scheduleVO.title}'," +
                " $KEY_MEMO = '${scheduleVO.memo}'," +
                " $KEY_TIME = '${scheduleVO.time}'" +
                " $KEY_IMGS = '${scheduleVO.getJoinedImgString()}'" +
                " WHERE $KEY_ID = ${scheduleVO.id!!};"
        mDb.execSQL(query)
    }

    fun getAll(): ArrayList<ScheduleVO> {
        val query = "SELECT * FROM $TABLE_NAME order by $KEY_TIME"
        val cursor = mDb.rawQuery(query, null)

        println("읽어올 스케줄 수 : ${cursor.count}")

        val scheduleVOList = LinkedList<ScheduleVO>()
        while (cursor.moveToNext()) {
            val scheduleVO = ScheduleVO().apply {
                id = cursor.getLong(0)
                title = cursor.getString(1)
                memo = cursor.getString(2)
                time = cursor.getString(3)
                setImgListFromJoinedString(cursor.getString(4))
            }

            println("cursor title : ${scheduleVO.title}")
            scheduleVOList.add(scheduleVO)
        }
        cursor.close()

        return ArrayList(scheduleVOList)
    }

    fun getPosition(scheduleVO: ScheduleVO): Int {
        val query = "SELECT * FROM $TABLE_NAME order by $KEY_TIME"
        val cursor = mDb.rawQuery(query, null)

        println("읽어올 메시지 수 : ${cursor.count}")

        var position = 0
        while (cursor.moveToNext()) {
            if (scheduleVO.id == cursor.getLong(0)) {
                cursor.close()
                return position
            } else {
                position++
            }
        }

        //찾을 데이터가 없음
        return -1
    }
}