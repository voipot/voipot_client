package voipot.jnu.conoz.com.voipotclient.util

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.runOnUiThread
import voipot.jnu.conoz.com.voipotclient.CommonApp

class WifiUtil{
    private var isLogin: Boolean = false
    private var wifiDelay = 0

    fun connectWifi(activity: AppCompatActivity, connListener: (result: Boolean)-> Unit) {
        val progress = activity.indeterminateProgressDialog(message = "Wifi 켜는 중!!")
        progress.show()
        val wifiManager = activity.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

        if (!wifiManager.isWifiEnabled) {
            wifiManager.isWifiEnabled = true
            progress.cancel()
            println("reConnect Wifi!")
            connectWifi(activity, connListener)
        } else {
            println("connect Wifi!")
            val wifiConfig = WifiConfiguration()

            wifiConfig.SSID = String.format("\"%s\"", CommonApp.guideID)
            wifiConfig.preSharedKey = String.format("\"%s\"", CommonApp.guidePW)

            val netId = wifiManager.addNetwork(wifiConfig)
            println("netId : $netId")

            wifiManager.disconnect()
            if (netId == -1) {
                progress.cancel()
                println("wifi 연결실패 : $netId")
                Toast.makeText(activity, "아이디 또는 패스워드가 틀렸습니다.", Toast.LENGTH_SHORT).show()
            } else {
                Thread {
                    Thread.sleep(1000)
                    if (wifiManager.enableNetwork(netId, true)) {
                        println("wifi 연결도전")
                        val receiver = object : BroadcastReceiver() {
                            override fun onReceive(context: Context, intent: Intent) {
                                println("receive")
                                wifiManager.connectionInfo.let {
                                    //ssid가 앞뒤로 따옴표 붙어서 나옴...
                                    if (it.ssid == "\"${CommonApp.guideID}\"" && wifiManager.connectionInfo.ipAddress != 0) {
                                        isLogin = true
                                        context.runOnUiThread {
                                            progress.cancel()
                                        }
                                        println("wifi 진짜 연결성공")
                                        connListener(true)
                                        (activity.application as CommonApp).setGuideIP()
                                    } else {
                                        println("wifi 진짜 연결실패 ${it.ssid}")
                                        println("wifi 진짜 연결실패 ${CommonApp.guideID}")
                                        //wifiDelay 다시 6초를 세서 프로그래스 종료
                                        wifiDelay = 0
                                    }
                                }
                            }
                        }
                        activity.registerReceiver(receiver, IntentFilter("android.net.wifi.STATE_CHANGE"))
                    }
                }.start()

                //6초 이내로 hotspot 켜지지 않으면 유효하지 않은 핫스팟
                Thread {
                    while (wifiDelay < 10) {
                        if (isLogin) {
                            break
                        }
                        Thread.sleep(1000)
                        wifiDelay++
                        println("wifiDelay : ${wifiDelay}초")
                    }
                    if (!isLogin) {
                        activity.runOnUiThread {
                            progress.cancel()
                            connListener(false)
                            wifiDelay = 0
                        }
                    }
                }.start()
            }
        }
    }
}